import { ThemeProvider } from 'styled-components';
import { ToastContainer } from 'react-toastify';
import { theme } from 'styles/theme';
import GlobalStyles from '../src/styles/global';

import 'react-toastify/dist/ReactToastify.css';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

export const decorators = [
  (Story) => {
    return (
      <ThemeProvider theme={theme}>
        <ToastContainer />
        <GlobalStyles />
        <Story />
      </ThemeProvider>
    )
  }
];
