import { ReactElement } from 'react';
import { render, RenderOptions } from '@testing-library/react';

import { ThemeProvider } from 'styled-components';
import { theme } from 'styles/theme';

type CustomRenderProps = Omit<RenderOptions, 'queries'>;

const customRender = (
  ui: ReactElement,
  { ...renderOptions }: CustomRenderProps = {}
) => {
  const rendered = render(
    <ThemeProvider theme={theme}>{ui}</ThemeProvider>,
    renderOptions
  );

  return {
    ...rendered,
    rerender: (
      ui: ReactElement,
      { ...rerenderOptions }: CustomRenderProps = {}
    ) => customRender(ui, { container: rendered.container, ...rerenderOptions })
  };
};

export * from '@testing-library/react';
export { customRender as render };
