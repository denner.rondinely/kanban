import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { ToastContainer } from 'react-toastify';
import SocketProvider from 'context/socket';
import Routes from 'routes';

import GlobalStyles from 'styles/global';
import { theme } from 'styles/theme';

import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <ToastContainer />
      <GlobalStyles />
      <SocketProvider>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </SocketProvider>
    </ThemeProvider>
  );
}

export default App;
