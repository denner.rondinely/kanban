export type Component = 'h1' | 'h2' | 'caption' | 'span' | 'p';

export type Variant =
  | 'heading-1'
  | 'heading-2'
  | 'subtitle'
  | 'body-1'
  | 'body-2'
  | 'caption';

export enum VariantType {
  'heading-1' = 'h1',
  'heading-2' = 'h1',
  subtitle = 'h2',
  'body-1' = 'p',
  'body-2' = 'p',
  'caption' = 'caption'
}
