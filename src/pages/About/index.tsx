import Button from 'components/atoms/Button';
import Icon from 'components/atoms/Icon';
import { useNavigate } from 'react-router-dom';

const About = () => {
  const navigate = useNavigate();
  return (
    <div>
      <div>Hello 👋, I am a About page.</div>
      <br />
      <Button
        color="salmon-medium"
        bg="gray-dark"
        onClick={() => navigate('/')}
        icon={<Icon icon="chevron-down" color="salmon-medium" size={16} />}
      >
        Go to Home
      </Button>
    </div>
  );
};

export default About;
