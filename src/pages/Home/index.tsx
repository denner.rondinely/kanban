import Button from 'components/atoms/Button';
import Icon from 'components/atoms/Icon';
import { useNavigate } from 'react-router-dom';

const Home = () => {
  const navigate = useNavigate();
  return (
    <div>
      <div>Hello 👋, I am a Home page.</div>
      <br />
      <Button
        bg="blue-dark"
        icon={<Icon icon="chevron-down" color="white" size={16} />}
        onClick={() => navigate('/about')}
      >
        Go to About
      </Button>
    </div>
  );
};

export default Home;
