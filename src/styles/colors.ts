export const colors = {
  primary: '#1976D2',
  blue: '#4285F4',
  'blue-light': '#8296E5',
  'blue-medium': '#2540AA',
  'blue-dark': '#1C307F',
  gray: '#cecece',
  'gray-light': '#F4F2F1',
  'gray-bright': '#F1F3F4',
  'gray-medium': '#999392',
  'gray-dark': '#575453',
  'salmon-light': '#FFA490',
  'salmon-medium': '#F28080',
  white: '#ffffff',
  black: '#000',
  transparent: '#0000'
};

export type ColorsType = keyof typeof colors;
