import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Icon, { IconProps } from '.';

export default {
  title: 'atoms/Icon',
  component: Icon
} as Meta;

export const Default: Story<IconProps> = (args) => <Icon {...args} />;

Default.args = {
  icon: 'chevron-down',
  size: 20,
  color: 'primary'
};
