import { ReactNode } from 'react';
import { Container } from './TextField.styles';

export interface TextFieldProps extends React.HTMLProps<HTMLInputElement> {
  icon?: ReactNode;
  visible?: boolean;
}

const TextField = ({
  icon,
  visible = true,
  style,
  ...props
}: TextFieldProps) => {
  if (!visible) {
    return null;
  }

  if (icon) {
    return (
      <Container style={style}>
        <input {...props} />
        {icon}
      </Container>
    );
  }

  return (
    <Container style={style}>
      <input {...props} />
    </Container>
  );
};

export default TextField;
