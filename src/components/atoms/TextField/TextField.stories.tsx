import { Story, Meta } from '@storybook/react/types-6-0';

import TextField, { TextFieldProps } from '.';
import Icon from '../Icon';

export default {
  title: 'atoms/TextField',
  component: TextField
} as Meta;

export const Default: Story<TextFieldProps> = () => {
  return <TextField placeholder="Search Request" type="text" />;
};

export const InputWithIcon: Story<TextFieldProps> = () => {
  return (
    <TextField
      placeholder="Search Request"
      type="text"
      icon={<Icon icon="search" size={20} color="gray-dark" />}
    />
  );
};
