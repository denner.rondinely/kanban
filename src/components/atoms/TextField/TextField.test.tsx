import TextField from './index';
import Icon from '../Icon';
import { render } from 'utils/tests/helpers';

describe('<TextField />', () => {
  it('should render default input', () => {
    const { container } = render(<TextField />);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should not render if visible is false ', () => {
    const { container } = render(<TextField visible={false} />);
    const input = container.querySelector('input');

    expect(input).not.toBeInTheDocument();
  });

  it('should render a input with a Icon', () => {
    const { container } = render(
      <TextField
        placeholder="Search Request"
        type="text"
        icon={<Icon icon="search" size={20} color="gray-dark" />}
      />
    );
    const input = container.querySelector('svg');

    expect(input).toBeInTheDocument();
  });
});
