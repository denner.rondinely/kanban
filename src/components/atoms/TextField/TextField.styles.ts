import styled from 'styled-components';

export const Container = styled.div`
  padding: 10px 16px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};

  input {
    border: none;
    outline: none;
    width: 100%;
    color: ${({ theme }) => theme.colors.black};
    font-size: ${({ theme }) => theme.font.sizes.small};
  }
`;
