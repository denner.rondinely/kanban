import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Wrapper, { WrapperProps } from '.';

export default {
  title: 'atoms/Wrapper',
  component: Wrapper
} as Meta;

export const Default: Story<WrapperProps> = () => (
  <Wrapper>
    <div>Children</div>
  </Wrapper>
);

export const Article: Story<WrapperProps> = () => (
  <Wrapper elementType={'article'}>
    <div>Article</div>
  </Wrapper>
);

export const Invisible: Story<WrapperProps> = () => (
  <Wrapper visible={false}>
    <div>Article</div>
  </Wrapper>
);

export const Row: Story<WrapperProps> = () => (
  <Wrapper direction="row">
    <div>Row</div>
  </Wrapper>
);

export const Column: Story<WrapperProps> = () => (
  <Wrapper direction="column">
    <div>Column</div>
  </Wrapper>
);

export const RowAndCenter: Story<WrapperProps> = () => (
  <Wrapper direction="row" fullWidth align="center">
    <div>RowAndCenter</div>
  </Wrapper>
);

export const RowAndEnd: Story<WrapperProps> = () => (
  <Wrapper direction="row" fullWidth align="end">
    <div>RowAndEnd</div>
  </Wrapper>
);
