import styled, { css } from 'styled-components';

import { WrapperProps } from './Wrapper';

const wrapperDirections = {
  row: css`
    flex-direction: row;
  `,
  column: css`
    flex-direction: column;
  `
};

const wrapperAlign = {
  center: css`
    justify-content: center;
    align-items: center;
  `,
  start: css`
    justify-content: start;
    align-items: flex-start;
  `,
  end: css`
    justify-content: end;
    align-items: flex-end;
  `
};

const ContainerAux = ({ elementType = 'div', ...props }: WrapperProps) => {
  const Component = elementType;
  return <Component {...props} />;
};

export const Container = styled(ContainerAux)<WrapperProps>`
  display: flex;
  ${({ align }) => !!align && wrapperAlign[align]}
  ${({ direction }) => !!direction && wrapperDirections[direction]}
${({ fullWidth }) =>
    fullWidth &&
    css`
      width: 100%;
    `}
`;
