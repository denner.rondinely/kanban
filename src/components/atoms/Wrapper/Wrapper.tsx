import { ReactNode } from 'react';
import { Container } from './Wrapper.styles';

export interface WrapperProps extends React.HTMLAttributes<HTMLDivElement> {
  visible?: boolean;
  elementType?: React.ElementType;
  children?: ReactNode;
  direction?: 'row' | 'column';
  align?: 'center' | 'start' | 'end';
  fullWidth?: boolean;
}

const Wrapper = ({
  visible = true,
  elementType,
  direction = 'row',
  align = 'start',
  fullWidth,
  children,
  ...props
}: WrapperProps) => {
  if (!visible) {
    return null;
  }

  return (
    <Container
      elementType={elementType}
      direction={direction}
      fullWidth={fullWidth}
      align={align}
      {...props}
    >
      {children}
    </Container>
  );
};

export default Wrapper;
