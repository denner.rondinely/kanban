import { screen } from '@testing-library/react';

import Wrapper from './index';
import { render } from 'utils/tests/helpers';

describe('<Wrapper />', () => {
  it('should render a div by default', () => {
    const { container } = render(<Wrapper>Hi!</Wrapper>);

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should not render if visible is false', () => {
    render(
      <Wrapper visible={false}>
        <span>Hi!</span>
      </Wrapper>
    );
    const textWrapper = screen.queryByText('Hi!');
    expect(textWrapper).not.toBeInTheDocument();
  });

  it('should  render if visible is true', () => {
    render(
      <Wrapper visible={true}>
        <span>Hi!</span>
      </Wrapper>
    );
    const textWrapper = screen.queryByText('Hi!');
    expect(textWrapper).toBeInTheDocument();
  });

  it('should render fullWidth version', () => {
    render(
      <Wrapper elementType={'article'} fullWidth>
        Hi
      </Wrapper>
    );
    expect(screen.getByRole('article')).toHaveStyle({
      width: '100%'
    });
  });

  it('should render a row direction version', () => {
    render(
      <Wrapper elementType={'article'} direction="row">
        <section>1</section>
        <section>2</section>
      </Wrapper>
    );
    expect(screen.getByRole('article')).toHaveStyle({
      flexDirection: 'row'
    });
  });

  it('should render a column direction version', () => {
    render(
      <Wrapper elementType={'article'} direction="column">
        <section>1</section>
        <section>2</section>
      </Wrapper>
    );
    expect(screen.getByRole('article')).toHaveStyle({
      flexDirection: 'column'
    });
  });

  it('should render a center version', () => {
    render(
      <Wrapper elementType={'article'} align="center" direction="row">
        <section>1</section>
        <section>2</section>
      </Wrapper>
    );
    expect(screen.getByRole('article')).toHaveStyle({
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    });
  });
});
