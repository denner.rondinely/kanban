import { Story, Meta } from '@storybook/react/types-6-0';

import Text, { TextProps } from '.';

export default {
  title: 'atoms/Text',
  component: Text,
  argTypes: {
    children: {
      type: 'string'
    }
  }
} as Meta;

export const Default: Story<TextProps> = (args) => <Text {...args} />;

Default.args = {
  children: 'https://services-sit.bees-platform.dev/api/cart-service/v3',
  color: 'gray-dark'
};
