import { screen } from '@testing-library/react';

import Text from './index';
import { render } from 'utils/tests/helpers';

describe('<Text />', () => {
  it('should render the heading h1', () => {
    const { container } = render(<Text component="h1">Text</Text>);

    expect(screen.getByRole('heading', { name: /Text/i })).toBeInTheDocument();

    expect(screen.getByRole('heading', { name: /Text/i })).toContainHTML('h1');

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render the heading h2', () => {
    const { container } = render(<Text component="h2">Text</Text>);

    expect(screen.getByRole('heading', { name: /Text/i })).toBeInTheDocument();

    expect(screen.getByRole('heading', { name: /Text/i })).toContainHTML('h2');
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render the span', () => {
    const { container } = render(<Text component="span">Text</Text>);

    expect(screen.getByText(/Text/i)).toBeInTheDocument();

    expect(screen.getByText(/Text/i)).toContainHTML('span');

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render the heading p', () => {
    const { container } = render(<Text component="p">Text</Text>);

    expect(screen.getByText(/Text/i)).toBeInTheDocument();

    expect(screen.getByText(/Text/i)).toContainHTML('p');

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render the color', () => {
    render(<Text color="blue">Text</Text>);

    expect(screen.getByText(/Text/i)).toBeInTheDocument();

    expect(screen.getByText(/Text/i)).toHaveStyle({
      color: '#4285F4'
    });
  });
});
