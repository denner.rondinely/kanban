import styled, { css, DefaultTheme } from 'styled-components';

import { TextProps } from '.';

type WrapperProps = { theme: DefaultTheme } & Omit<TextProps, 'children'>;

const includeNowrap = css`
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const getVariant = {
  'heading-1': ({
    theme,
    nowrap,
    align = 'center',
    color = 'blue-dark',
    weight = 'bold'
  }: WrapperProps) => css`
    color: ${theme.colors[color]};
    font-style: normal;
    font-weight: ${theme.font[weight]};
    font-size: ${theme.font.sizes.xxlarge};
    line-height: 3.9rem;
    letter-spacing: -1.5px;
    text-align: ${align};
    ${nowrap && includeNowrap}
  `,
  'heading-2': ({
    theme,
    nowrap,
    align = 'start',
    color = 'blue-dark',
    weight = 'bold'
  }: WrapperProps) => css`
    color: ${theme.colors[color]};
    font-style: normal;
    font-weight: ${theme.font[weight]};
    font-size: ${theme.font.sizes.xlarge};
    line-height: 3.8rem;
    text-align: ${align};
    ${nowrap && includeNowrap}
  `,
  'body-1': ({
    theme,
    nowrap,
    align = 'start',
    color = 'gray-dark',
    weight = 'normal'
  }: WrapperProps) => css`
    font-style: normal;
    font-weight: ${theme.font[weight]};
    font-size: ${theme.font.sizes.small};
    line-height: 2rem;
    color: ${theme.colors[color]};
    text-align: ${align};
    ${nowrap && includeNowrap}
  `,
  'body-2': ({
    theme,
    nowrap,
    align = 'start',
    color = 'gray-dark',
    weight = 'normal'
  }: WrapperProps) => css`
    font-style: normal;
    font-weight: ${theme.font[weight]};
    font-size: ${theme.font.sizes.xsmall};
    line-height: 1.6rem;
    color: ${theme.colors[color]};
    text-align: ${align};
    ${nowrap && includeNowrap}
  `,
  subtitle: ({
    theme,
    nowrap,
    align = 'center',
    color = 'gray-medium',
    weight = 'bold'
  }: WrapperProps) =>
    css`
      font-style: normal;
      font-weight: ${theme.font[weight]};
      font-size: ${theme.font.sizes.medium};
      line-height: 2.4rem;
      text-align: ${align};
      color: ${theme.colors[color]};
      ${nowrap && includeNowrap}
    `,
  caption: ({
    theme,
    nowrap,
    align = 'start',
    color = 'gray-dark',
    weight = 'normal'
  }: WrapperProps) =>
    css`
      font-style: normal;
      font-weight: ${theme.font[weight]};
      font-size: ${theme.font.sizes.xxsmall};
      line-height: 2rem;
      text-align: ${align};
      color: ${theme.colors[color]};
      ${nowrap && includeNowrap}
    `
};

export const h1 = styled.h1<WrapperProps>`
  ${({ variant = 'heading-1', ...props }: WrapperProps) =>
    getVariant[variant](props)}
`;
export const h2 = styled.h2<WrapperProps>`
  ${({ variant = 'subtitle', ...props }: WrapperProps) =>
    getVariant[variant](props)}
`;
export const span = styled.span<WrapperProps>`
  ${({ variant = 'body-1', ...props }: WrapperProps) =>
    getVariant[variant](props)}
`;
export const p = styled.p<WrapperProps>`
  ${({ variant = 'body-1', ...props }) => getVariant[variant](props)}
`;
export const caption = styled.caption<WrapperProps>`
  ${({ variant = 'caption', ...props }) => getVariant[variant](props)}
`;
