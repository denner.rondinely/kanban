import { ReactNode } from 'react';

import { ColorsType } from 'styles/colors';

import { Component, Variant, VariantType } from 'types/text';

import * as S from './Text.styles';

export type TextProps = {
  variant?: Variant;
  component?: Component;
  color?: ColorsType;
  nowrap?: boolean;
  align?: 'start' | 'center' | 'end';
  weight?: 'light' | 'normal' | 'bold';
  children?: ReactNode | string;
  role?: string;
};

const Text = ({
  children,
  variant = 'body-1',
  component,
  align,
  weight,
  color,
  nowrap,
  role
}: TextProps) => {
  const Component = S[component ?? VariantType[variant]];

  return (
    <Component
      variant={variant}
      align={align}
      weight={weight}
      color={color}
      nowrap={nowrap}
      role={role}
    >
      {children}
    </Component>
  );
};

export default Text;
