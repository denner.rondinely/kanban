import { Story, Meta } from '@storybook/react/types-6-0';

import Table, { TableProps } from '.';

export default {
  title: 'atoms/Table',
  component: Table,
  argTypes: {
    fullWidth: {
      type: 'boolean'
    },
    labels: {
      required: true
    },
    items: {
      required: true
    },
    onClickRow: {
      type: 'function'
    }
  }
} as Meta;

export const Default: Story<TableProps> = (args) => {
  return <Table {...args} />;
};

const item = {
  locolibName: 'cart_service_requester_v3 ',
  host: 'https://services-sit.bees-platform.dev/api/cart-service/v3',
  statusCode: 200,
  method: 'get',
  date: '2022-08-12 - 12:01'
};

Default.args = {
  items: [item, item, item, item, item, item, item, item, item, item],
  labels: ['Locolib Name', 'Host', 'Status Code', 'Method', 'Date'],
  fullWidth: false,
  onClickRow: (item) => console.log(item)
};
