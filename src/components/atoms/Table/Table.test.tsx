import { fireEvent, screen } from '@testing-library/react';

import Table from './index';
import { render } from 'utils/tests/helpers';

const item = {
  locolibName: 'cart_service_requester_v3 ',
  host: 'https://services-sit.bees-platform.dev/api/cart-service/v3',
  statusCode: 200,
  method: 'get',
  date: '2022-08-12 - 12:01'
};

const labels = ['Locolib Name', 'Host', 'Status Code', 'Method', 'Date'];

describe('<Table />', () => {
  it('should render table', () => {
    const { container } = render(
      <Table labels={labels} items={[item, item]} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should render full width table', () => {
    render(<Table fullWidth={true} labels={labels} items={[item, item]} />);
    expect(screen.getByRole('table')).toHaveStyle({
      width: '100%'
    });
  });

  it('should render 2 rows on the table', () => {
    const { container } = render(
      <Table labels={labels} items={[item, item]} />
    );

    expect(2).toBe(container.querySelectorAll('tbody > tr').length);
  });

  it('should call function if click in row', () => {
    const onClick = jest.fn();

    const { container } = render(
      <Table labels={labels} items={[item, item]} onClickRow={onClick} />
    );

    const row = container.querySelector('tbody > tr');
    if (row) fireEvent.click(row);

    expect(2).toBe(container.querySelectorAll('tbody > tr').length);
  });
});
