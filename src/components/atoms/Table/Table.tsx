import { useCallback } from 'react';
import { TableStyled } from './Table.styles';

export interface TableProps {
  items: object[];
  labels: string[];
  fullWidth?: boolean;
  onClickRow?: (item: object) => void;
}

const Table = ({ items, labels, fullWidth, onClickRow }: TableProps) => {
  const renderThead = useCallback(
    () => (
      <thead>
        <tr>
          {labels.map((label, index) => (
            <th key={index}>{label}</th>
          ))}
        </tr>
      </thead>
    ),
    [labels]
  );

  const renderTBody = useCallback(
    () => (
      <tbody>
        {items.map((item, index) => (
          <tr key={index} onClick={() => onClickRow && onClickRow(item)}>
            {(Object.keys(item) as (keyof typeof item)[]).map((key, index) => (
              <td key={index}>{item[key]}</td>
            ))}
          </tr>
        ))}
      </tbody>
    ),
    [items, onClickRow]
  );

  return (
    <TableStyled fullWidth={fullWidth} isClickable={!!onClickRow}>
      {renderThead()}
      {renderTBody()}
    </TableStyled>
  );
};

export default Table;
