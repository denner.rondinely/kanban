import styled, { css } from 'styled-components';

type TableStyledProps = {
  fullWidth?: boolean;
  isClickable?: boolean;
};

export const TableStyled = styled.table<TableStyledProps>`
  border-collapse: collapse;
  font-size: ${({ theme }) => theme.font.sizes.xsmall};
  width: ${({ fullWidth }) => fullWidth && '100%'};

  th,
  td {
    padding: 8px;
  }

  th:not(:first-child),
  td:not(:first-child) {
    border-left: 1px solid ${({ theme }) => theme.colors.gray};
  }

  thead th {
    background-color: ${({ theme }) => theme.colors['gray-light']};
  }

  /* even lines*/
  tbody tr:nth-child(even) {
    background-color: ${({ theme }) => theme.colors['gray-light']};
  }
  /* odd lines */
  tbody tr:nth-child(odd) {
    background-color: ${({ theme }) => theme.colors.white};
  }

  ${({ isClickable }) =>
    isClickable &&
    css`
      td:active {
        opacity: 0.8;
      }

      td:hover {
        cursor: pointer;
      }
    `}
`;
