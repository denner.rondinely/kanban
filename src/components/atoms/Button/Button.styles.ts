import styled, { css, DefaultTheme } from 'styled-components';

import { ButtonProps } from '.';

type WrapperProps = { hasIcon: boolean } & Pick<
  ButtonProps,
  'size' | 'fullWidth' | 'bg' | 'color' | 'border'
>;

const wrapperModifiers = {
  small: (theme: DefaultTheme) => css`
    height: 2.8rem;
    font-size: ${theme.font.sizes.xxsmall};
    line-height: 2.4rem;
    padding: 0 ${theme.spacings.small};
  `,
  medium: (theme: DefaultTheme) => css`
    height: 3.6rem;
    font-size: ${theme.font.sizes.xsmall};
    line-height: 1.8rem;
    padding: 0 ${theme.spacings.small};
  `,
  large: (theme: DefaultTheme) => css`
    height: 4.4rem;
    font-size: ${theme.font.sizes.xsmall};
    line-height: 3.6rem;
    padding: 0 ${theme.spacings.small};
  `,

  withIcon: (theme: DefaultTheme) => css`
    display: inline-flex;
    align-items: center;
    justify-content: center;

    svg {
      & + span {
        margin-left: ${theme.spacings.xxsmall};
      }
    }
  `,
  fullWidth: () => css`
    width: 100%;
  `
};

export const Wrapper = styled.button<WrapperProps>`
  cursor: pointer;
  white-space: nowrap;
  transition: all 0.3s;
  text-transform: uppercase;
  letter-spacing: 1.25px;
  border: none;

  ${({
    theme,
    size,
    fullWidth,
    hasIcon,
    color = 'white',
    bg = 'blue',
    border
  }) =>
    css`
      border-radius: ${border ? '4.4rem' : theme.border.radius.small};
      color: ${theme.colors[color]};
      background-color: ${theme.colors[bg]};
      font-weight: ${theme.font.bold};

      ${!!size && wrapperModifiers[size](theme)};
      ${!!fullWidth && wrapperModifiers.fullWidth()};
      ${!!hasIcon && wrapperModifiers.withIcon(theme)};
      &:disabled {
        cursor: not-allowed;
        color: ${theme.colors['gray']};
        background-color: ${theme.colors['gray-light']};
      }
    `}
`;
