import { Story, Meta } from '@storybook/react/types-6-0';
import styled from 'styled-components';

import Button, { ButtonProps } from '.';

export default {
  title: 'atoms/Button',
  component: Button
} as Meta;

const Wrapper = styled.div`
  display: flex;
  gap: 1.6rem;
`;

export const Default: Story<ButtonProps> = (args) => (
  <Button {...args}>Select Nfa joker build</Button>
);

export const Border: Story<ButtonProps> = (args) => (
  <Wrapper>
    <Button {...args}>Export all</Button>
    <Button {...args}>Clear</Button>
  </Wrapper>
);

Border.args = {
  border: true
};

export const BorderSmall: Story<ButtonProps> = (args) => (
  <Wrapper>
    <Button {...args}>copy curl</Button>
    <Button {...args}>export request</Button>
  </Wrapper>
);

BorderSmall.args = {
  border: true,
  size: 'small'
};
