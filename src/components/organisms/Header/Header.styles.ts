import styled, { css } from 'styled-components';

export const Wrapper = styled.header`
  display: flex;
  flex-direction: column;
`;

export const HeaderTitle = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors['gray-bright']};
    padding: ${theme.spacings.small};
  `}
`;

export const HeaderInteraction = styled.div`
  display: flex;
  gap: 18.3rem;
  ${({ theme }) => css`
    background-color: ${theme.colors.white};
    padding: ${theme.spacings.minimal} ${theme.spacings.xsmall}
      ${theme.spacings.xsmall} ${theme.spacings.minimal};
  `}
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  gap: 1.1rem;
`;
