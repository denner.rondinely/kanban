import Header from './index';
import { render } from 'utils/tests/helpers';

describe('<Header />', () => {
  it('should render the medium size by default', () => {
    const { container } = render(<Header />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
