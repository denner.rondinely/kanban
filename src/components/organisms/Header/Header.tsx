import Button from 'components/atoms/Button';
import Icon from 'components/atoms/Icon';
import Text from 'components/atoms/Text';
import TextField from 'components/atoms/TextField';
import * as S from './Header.styles';

const Header = () => {
  return (
    <S.Wrapper>
      <S.HeaderTitle>
        <Text color="primary" component="h1" variant="body-1" weight="bold">
          Joker Debuger
        </Text>
      </S.HeaderTitle>
      <S.HeaderInteraction>
        <TextField
          style={{ width: '100%' }}
          placeholder="Search Request"
          type="text"
          icon={<Icon icon="search" size={20} color="gray-dark" />}
        />
        <S.ButtonsWrapper>
          <Button border>Export all</Button>
          <Button border>Clear</Button>
        </S.ButtonsWrapper>
      </S.HeaderInteraction>
    </S.Wrapper>
  );
};

export default Header;
