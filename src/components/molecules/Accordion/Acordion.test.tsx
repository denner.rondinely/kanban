import { screen, fireEvent } from '@testing-library/react';

import Accordion from './index';
import { render } from 'utils/tests/helpers';

describe('<Accordion />', () => {
  it('should render the medium size by default', () => {
    const { container } = render(<Accordion text="test">BEES!</Accordion>);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('show show content when clicked in Accordion', () => {
    const text = 'BEES!';
    const { rerender } = render(<Accordion text="test">{text}</Accordion>);

    expect(screen.getByTestId('accordion-body')).toHaveStyle({
      maxHeight: '0'
    });

    fireEvent.click(screen.getByTestId('accordion-header'));
    rerender(<Accordion text="test">{text}</Accordion>);
    expect(screen.getByTestId('accordion-body')).toHaveStyle({
      maxHeight: '100vh'
    });
  });
});
