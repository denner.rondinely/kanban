import { Story, Meta } from '@storybook/react/types-6-0';
import Text from 'components/atoms/Text';

import Accordion, { AccordionProps } from '.';

export default {
  title: 'molecules/Accordion',
  component: Accordion
} as Meta;

export const Default: Story<AccordionProps> = (args) => (
  <>
    <Accordion {...args}>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
    </Accordion>
    <Accordion {...args}>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
      <Text>accountId: 03ff9f7b-243a-4f93-97d5-70b70bd68def</Text>
    </Accordion>
  </>
);

Default.args = {
  text: 'Headers'
};
