import * as S from './Accordion.styles';

import Icon from 'components/atoms/Icon';
import Text from 'components/atoms/Text';
import { useState } from 'react';

export interface AccordionProps {
  text: string;
  children: JSX.Element[] | JSX.Element | string;
}

const Accordion = ({ text, children }: AccordionProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <S.Container>
      <S.Header
        data-testid="accordion-header"
        show={isOpen}
        onClick={() => setIsOpen((t) => !t)}
      >
        <Text color="black" component="span">
          {text}
        </Text>
        <Icon size={18} icon={'chevron-down'} color="black" />
      </S.Header>

      <S.Body data-testid="accordion-body" show={isOpen}>
        {children}
      </S.Body>
    </S.Container>
  );
};

export default Accordion;
