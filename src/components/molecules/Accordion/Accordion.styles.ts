import styled, { css } from 'styled-components';

export const Container = styled.div``;

type HeaderProps = {
  show: boolean;
};

export const Header = styled.div<HeaderProps>`
  padding: 4px 16px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-right: 10px;
    animation: all 0.3s ease-in-out;

    ${({ show }) =>
      show &&
      css`
        transform: rotate(180deg);
      `};
  }

  span {
    font-weight: ${({ theme }) => theme.font.bold};
  }

  &:hover {
    cursor: pointer;
  }
`;

type BodyProps = {
  show: boolean;
};

export const Body = styled.div<BodyProps>`
  width: 100%;
  overflow: hidden;
  max-height: ${({ show }) => (show ? ' 100vh' : '0')};
  transform-origin: top;
  transition: all 0.35s ease-in-out;
`;
