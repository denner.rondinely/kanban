import { toast } from 'react-toastify';
import Button from 'components/atoms/Button';
import Icon from 'components/atoms/Icon';
import { ColorsType } from 'styles/colors';

export interface CopyButtonProps {
  value?: string;
  size?: number | string;
  color?: ColorsType;
  iconColor?: ColorsType;
}

const CopyButton = ({
  value = '',
  size = 20,
  color = 'transparent',
  iconColor = 'black'
}: CopyButtonProps) => {
  const handleClick = () => {
    navigator.clipboard.writeText(value);
    toast.info('Copiado!');
  };

  return (
    <Button bg={color} onClick={handleClick}>
      <Icon icon="copy" size={size} color={iconColor} />
    </Button>
  );
};

export default CopyButton;
