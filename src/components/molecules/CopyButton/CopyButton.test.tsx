import { fireEvent, screen } from '@testing-library/react';

import CopyButton from './index';
import { render } from 'utils/tests/helpers';

Object.assign(navigator, {
  clipboard: {
    writeText: jest.fn()
  }
});

describe('<CopyButton />', () => {
  it('should render the medium size by default', () => {
    const { container } = render(<CopyButton />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('should copy text to clipboard', () => {
    const text = 'Copiar texto!';
    render(<CopyButton value={text} />);
    fireEvent.click(screen.getByRole('button'));
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith(text);
  });
});
