import { Story, Meta } from '@storybook/react/types-6-0';

import CopyButton, { CopyButtonProps } from '.';

export default {
  title: 'molecules/CopyButton',
  component: CopyButton
} as Meta;

export const Default: Story<CopyButtonProps> = (args) => (
  <CopyButton {...args} />
);

Default.args = {
  value: 'Copiar texto!'
};
