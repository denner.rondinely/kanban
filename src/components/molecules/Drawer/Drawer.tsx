import * as S from './Drawer.styles';

import { ReactNode } from 'react';

export interface DrawerProps {
  children?: ReactNode;
  open?: boolean;
}

const Drawer = ({ children, open }: DrawerProps) => {
  return <S.Wrapper open={open}>{children}</S.Wrapper>;
};

export default Drawer;
