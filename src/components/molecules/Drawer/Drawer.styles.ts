import styled, { css, keyframes } from 'styled-components';
import { DrawerProps } from '.';

type ContainerProps = Pick<DrawerProps, 'open'>;

const slideUp = keyframes`
  {
    0% {
        transform: translatex(100%);
    }
    100% {
        transform: translatex(0px);
    }
  }
`;

export const Wrapper = styled.div<ContainerProps>`
  position: fixed;
  right: 0;
  top: 0;
  bottom: 0;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;
  max-width: 91.1rem;
  height: 100%;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  ${({ theme, open }) => css`
    display: ${open ? 'flex' : 'none'};
    z-index: ${theme.layers.modal};
    border: 1px solid ${theme.colors.gray};
    background-color: ${theme.colors.white};
    animation: ${slideUp} 0.3s ease-in-out;
  `}
`;
