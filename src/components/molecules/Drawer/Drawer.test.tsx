import Drawer from './index';
import { render } from 'utils/tests/helpers';

describe('<Drawer />', () => {
  it('should render Drawer', () => {
    const { container } = render(<Drawer />);

    expect(container.firstChild).toMatchSnapshot();
  });
});
