import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import Drawer, { DrawerProps } from '.';

export default {
  title: 'molecules/Drawer',
  component: Drawer
} as Meta;

export const Default: Story<DrawerProps> = (args) => (
  <Drawer {...args}>Hello 👋, I am a Drawer component.</Drawer>
);

Default.args = {
  open: true
};
