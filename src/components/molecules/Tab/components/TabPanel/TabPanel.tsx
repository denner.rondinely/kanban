import Wrapper from 'components/atoms/Wrapper';
import { ReactNode } from 'react';

import { useTab } from '../TabContext';

type TabPanelProps = {
  value: number;
  children?: ReactNode;
};

const TabPanel = ({ value, children, ...props }: TabPanelProps) => {
  const { currentValue } = useTab();

  if (currentValue !== value) {
    return null;
  }

  return <Wrapper {...props}>{children}</Wrapper>;
};

export default TabPanel;
