import { ReactNode, createContext, useContext } from 'react';

type TabContextProps = {
  value: string | number | null;
  children: ReactNode;
};

type ContextType = {
  currentValue: string | number | null;
};

const Context = createContext<ContextType>({ currentValue: null });

const TabContext = ({ value, children }: TabContextProps) => {
  return (
    <Context.Provider value={{ currentValue: value }}>
      {children}
    </Context.Provider>
  );
};

export const useTab = () => {
  const context = useContext(Context);

  if (!context) {
    throw new Error('useTab must be used within a TabContext');
  }

  return context;
};

export default TabContext;
