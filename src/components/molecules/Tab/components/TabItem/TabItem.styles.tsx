import styled from 'styled-components';

type ContainerProps = {
  isActivated: boolean;
};

export const Container = styled.div<ContainerProps>`
  margin-left: 16px;
  margin-right: 16px;
  padding: 8px 16px;
  box-sizing: border-box;

  border-bottom: ${({ isActivated, theme }) =>
    isActivated && `4px solid ${theme.colors.blue}`};

  &:hover {
    cursor: pointer;
  }
`;
