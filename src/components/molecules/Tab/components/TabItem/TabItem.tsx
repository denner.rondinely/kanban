import Text from 'components/atoms/Text';

import * as S from './TabItem.styles';

import { useTab } from '../TabContext';

type TabItemProps = {
  label: string;
  value: number;
  onChangeTab?: (value: number) => void;
};

const TabItem = ({ label, value, onChangeTab, ...props }: TabItemProps) => {
  const { currentValue } = useTab();

  const isActivated = currentValue === value;

  return (
    <S.Container
      isActivated={isActivated}
      onClick={() => onChangeTab && onChangeTab(value)}
      {...props}
    >
      <Text color={isActivated ? 'black' : 'gray-dark'}>{label}</Text>
    </S.Container>
  );
};

TabItem.displayName = 'TabItem';

export default TabItem;
