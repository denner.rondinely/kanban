import * as React from 'react';

import Wrapper from 'components/atoms/Wrapper';

type TabListProps = {
  children: JSX.Element[] | JSX.Element;
  direction?: 'row' | 'column';
  onChangeTab: (value: number) => void;
};

const TabList = ({
  children,
  direction = 'row',
  onChangeTab,
  ...props
}: TabListProps) => {
  return (
    <Wrapper direction={direction} {...props}>
      {React.Children.map(children, (child) => {
        const props =
          child.type.displayName === 'TabItem' ? { onChangeTab } : {};
        return React.cloneElement(child, props);
      })}
    </Wrapper>
  );
};

export default TabList;
