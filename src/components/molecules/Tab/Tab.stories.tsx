import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import { TabContext, TabList, TabItem, TabPanel } from '.';

export default {
  title: 'molecules/Tab',
  component: TabContext,
  subcomponents: { TabList, TabItem, TabPanel }
} as Meta;

export const Default: Story = () => {
  const [value, setValue] = React.useState<string | number>('1');

  const handleChange = (newValue: string | number) => {
    setValue(newValue);
  };

  return (
    <TabContext value={value}>
      <TabList onChangeTab={handleChange}>
        <TabItem label="Item One" value={1} />
        <TabItem label="Item Two" value={2} />
        <TabItem label="Item Three" value={3} />
      </TabList>
      <TabPanel value={1}>Item One</TabPanel>
      <TabPanel value={2}>Item Two</TabPanel>
      <TabPanel value={3}>Item Three</TabPanel>
    </TabContext>
  );
};
