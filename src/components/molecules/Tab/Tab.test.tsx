import { screen, fireEvent } from '@testing-library/react';

import { TabContext, TabList, TabItem, TabPanel } from '.';

import { render } from 'utils/tests/helpers';

describe('<Tab />', () => {
  it('should render the medium size by default', () => {
    const value = 0;
    const handleChange = jest.fn();

    const { container } = render(
      <TabContext value={value}>
        <TabList onChangeTab={handleChange}>
          <TabItem label="Item One" value={1} />
          <TabItem label="Item Two" value={2} />
          <TabItem label="Item Three" value={3} />
        </TabList>
        <TabPanel value={1}>Item One</TabPanel>
        <TabPanel value={2}>Item Two</TabPanel>
        <TabPanel value={3}>Item Three</TabPanel>
      </TabContext>
    );

    expect(container.firstChild).toMatchSnapshot();
  });

  it('should call onChangeTab when click on tab item', () => {
    const value = 0;
    const handleChange = jest.fn();

    render(
      <TabContext value={value}>
        <TabList onChangeTab={handleChange}>
          <TabItem data-testid="item-one" label="Item One" value={1} />
          <TabItem data-testid="item-two" label="Item Two" value={2} />
        </TabList>
        <TabPanel value={1}>Item One</TabPanel>
        <TabPanel value={2}>Item Two</TabPanel>
      </TabContext>
    );

    fireEvent.click(screen.getByTestId('item-two'));

    expect(handleChange).toHaveBeenCalled();
    expect(handleChange).toHaveBeenCalledWith(2);
  });

  it('should show correct Panel', () => {
    const handleChange = jest.fn();

    const Tab = ({ value }: { value: number }) => {
      return (
        <TabContext value={value}>
          <TabList onChangeTab={handleChange}>
            <TabItem label="Item One" value={1} />
            <TabItem data-testid="item-two" label="Item Two" value={2} />
          </TabList>
          <TabPanel data-testid="panel-one" value={1}>
            Item One
          </TabPanel>
          <TabPanel data-testid="panel-two" value={2}>
            Item Two
          </TabPanel>
        </TabContext>
      );
    };

    const { rerender } = render(<Tab value={1} />);
    expect(screen.queryByTestId('panel-two')).not.toBeInTheDocument();

    rerender(<Tab value={2} />);

    expect(screen.queryByTestId('panel-one')).not.toBeInTheDocument();
    expect(screen.queryByTestId('panel-two')).toBeInTheDocument();
  });

  it('should show border bottom on active tab', () => {
    const handleChange = jest.fn();

    const Tab = ({ value }: { value: number }) => {
      return (
        <TabContext value={value}>
          <TabList onChangeTab={handleChange}>
            <TabItem data-testid="item-one" label="Item One" value={1} />
            <TabItem data-testid="item-two" label="Item Two" value={2} />
          </TabList>
          <TabPanel value={1}>Item One</TabPanel>
          <TabPanel value={2}>Item Two</TabPanel>
        </TabContext>
      );
    };

    const { rerender } = render(<Tab value={1} />);

    expect(screen.getByTestId('item-one')).toHaveStyle({
      'border-bottom': '4px solid #4285F4'
    });

    rerender(<Tab value={2} />);

    expect(screen.getByTestId('item-two')).toHaveStyle({
      'border-bottom': '4px solid #4285F4'
    });
    expect(screen.getByTestId('item-one')).not.toHaveStyle({
      'border-bottom': '4px solid #4285F4'
    });
  });
});
