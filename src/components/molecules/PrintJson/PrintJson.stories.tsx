import { Story, Meta } from '@storybook/react/types-6-0';

import PrintJson, { PrintJsonProps } from '.';

export default {
  title: 'molecules/PrintJson',
  component: PrintJson
} as Meta;

export const Default: Story<PrintJsonProps> = (args) => <PrintJson {...args} />;

Default.args = {
  data: {
    message: 'Put some JSON in the text box.',
    error: null,
    year: 2022,
    ios: false,
    space: '🪐🚀✨',
    fancy: 'https://json5.org/?8'
  }
};
