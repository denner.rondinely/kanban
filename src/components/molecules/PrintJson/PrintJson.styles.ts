import styled from 'styled-components';

export const Code = styled.pre`
  background-color: ${({ theme }) => theme.colors['gray-light']};
  padding: 16px;
  border: 1px solid ${({ theme }) => theme.colors.gray};
  overflow: hidden;

  /* Colors */
  .json-container {
    background-color: white;
  }
  .json-key {
    color: teal;
  }
  .json-string,
  .json-number,
  .json-boolean,
  .json-null {
    color: ${({ theme }) => theme.colors['blue-dark']};
  }

  .json-mark {
    color: ${({ theme }) => theme.colors['gray-dark']};
  }
`;
