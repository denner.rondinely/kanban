import PrintJson from './index';
import { render } from 'utils/tests/helpers';

const json = {
  data: {
    message: 'Put some JSON in the text box.',
    error: null,
    year: 2022,
    ios: false,
    space: '🪐🚀✨',
    fancy: 'https://json5.org/?8'
  }
};

describe('<PrintJson />', () => {
  it('should render the medium size by default', () => {
    const { container } = render(<PrintJson data={json} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
