import { useMemo } from 'react';

import { prettyPrintJson } from 'pretty-print-json';

import { Code } from './PrintJson.styles';

export interface PrintJsonProps {
  data: object;
}

const PrintJson = ({ data }: PrintJsonProps) => {
  const prettyJson = useMemo(() => {
    return prettyPrintJson.toHtml(data);
  }, [data]);

  return <Code dangerouslySetInnerHTML={{ __html: prettyJson }} />;
};

export default PrintJson;
