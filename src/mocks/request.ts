const requestData = {
  request: [
    {
      label: 'url',
      content:
        'https://services-sit.bees-platform.dev/v1/order-transparency-service/?accountId=10d4f5dc-f261-41b1-90fe-c244da6e3fba&orderNumber=SIT0000021&page=0&pageSize=1'
    },
    { label: 'method', content: 'GET' },
    { label: 'Authorization', content: 'iOiJKV1QiLCJhbGciOiJSUzI1' },
    { label: 'country', content: 'GB' },
    {
      label: 'headers',
      content: [
        { label: 'Authorization1', content: 'iOiJKV1QiLCJhbGciOiJSUzI1' },
        { label: 'country2', content: 'GB' },
        { label: 'requestTraceId3', content: 'NFA_1650378126127' }
      ]
    },
    { label: 'requestTraceId', content: 'NFA_1650378126127' }
  ],
  response: {
    content: [
      {
        accountId: '10d4f5dc-f261-41b1-90fe-c244da6e3fba',
        paymentMethod: 'CASH',
        placementDate: '2022-04-12',
        placementDateTime: '2022-04-12T08:04:59+00:00',
        channel: 'B2B_WEB',
        status: 'PENDING',
        deliveryDate: '2022-04-14',
        discount: 0,
        subtotal: 180,
        deposit: 36,
        tax: 12,
        total: 228,
        deliveryCenter: 'GB05',
        isCancellable: false,
        orderNumber: 'SIT0000021',
        originalTotal: 228,
        empties: {},
        hasChanges: false,
        isEditable: false,
        items: [
          {
            id: '057b5f79-9a52-335d-aabe-95ca9c9383a1',
            discount: 0,
            price: 32,
            quantity: 6,
            freeGood: false,
            sku: '77727',
            subtotal: 180,
            tax: 12,
            total: 192,
            originalQuantity: 6,
            originalTotal: 192,
            originalPrice: 32,
            name: 'BECKS 6X4X440ML CAN',
            image:
              'https://www.ab-inbev.com/img/redesign/news-stories/netzero/Net_Zero_Banner_1440x472.jpg',
            container: {
              name: 'LATA',
              itemSize: 440,
              size: 440,
              unitOfMeasurement: 'ml',
              returnable: true
            },
            measureUnit: 'ml',
            vendorItemId: '77727',
            taxes: [
              {
                taxId: '14234325',
                taxAmount: 12
              }
            ],
            empty: false
          }
        ],
        taxes: [
          {
            taxId: '14234325',
            taxAmount: 12
          }
        ],
        vendor: {
          id: '65be2938-6bac-400b-bce2-482e138a48e0',
          accountId: '0010519777'
        },
        interest: {
          total: 0,
          loanDeduction: 0,
          detail: []
        }
      }
    ],
    pagination: {
      page: 0,
      pageSize: 1,
      totalCount: 1,
      totalPages: 1
    }
  }
};

export default requestData;
